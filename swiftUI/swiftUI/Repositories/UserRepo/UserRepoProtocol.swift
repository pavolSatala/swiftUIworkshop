//
//  UserRepoProtocol.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

protocol UserRepoProtocol
{
    func startUserListening()
    func stopUserListening()
    
    var userSubject : CurrentValueSubject<Int?, Never> { get }
}
