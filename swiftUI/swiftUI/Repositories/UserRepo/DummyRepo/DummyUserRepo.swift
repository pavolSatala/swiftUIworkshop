//
//  DummyUserRepo.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

class DummyUserRepo : UserRepoProtocol
{
    // Protocol
    var userSubject: CurrentValueSubject<Int?, Never> = CurrentValueSubject<Int?, Never>(nil)
   
    internal var timer : Timer?
    internal let NBR_DUMMY_USER  = 5
    
    func startUserListening() {
        self.downloadUser()
        self.timer = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
    }
    
    func stopUserListening() {
        self.timer?.invalidate()
    }
    
    @objc func timerTick()
    {
        self.downloadUser()
    }
}
