//
//  DummyUserRepo+DummyDao.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DummyUserRepo
{
    internal func downloadUser() {
        let delay = Int.random(in: 1..<5)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
            self.userSubject.send(Int.random(in: 1..<self.NBR_DUMMY_USER))
        }
    }
}
