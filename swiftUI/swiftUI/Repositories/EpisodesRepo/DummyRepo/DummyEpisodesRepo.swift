//
//  DummyEpisodesRepo.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

class DummyEpisodesRepo : EpisodesRepoProtocol
{
    var episodesSubject: CurrentValueSubject<[Episode]?, Never> = CurrentValueSubject<[Episode]?, Never>(nil)
    
    internal var timer : Timer?
    internal let NBR_DUMMY_USER  = 5
    
    func startEpisodesListening() {
        self.downloadEpisode()
        self.timer = Timer.scheduledTimer(timeInterval: 23.0, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
    }
    
    func stopEpisodesListening() {
        self.timer?.invalidate()
    }
    
    @objc func timerTick()
    {
        self.downloadEpisode()
    }
}
