//
//  EpisodesRepoProtocol.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

protocol EpisodesRepoProtocol
{
    func startEpisodesListening()
    func stopEpisodesListening()
    
    var episodesSubject : CurrentValueSubject<[Episode]?, Never> { get }
}
