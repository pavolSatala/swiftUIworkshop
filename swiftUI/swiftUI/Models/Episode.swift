//
//  Episode.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

enum EpisodeType : String
{
    case normal = "normal"
    case bradycardia = "bradycardia"
    case tachycardia = "tachycardia"
    case failure = "failure"
}

class Episode
{
    var date        : Date?
    var avgPulse    : Float?
    var type        : EpisodeType?
    var userId      : Int?
}
