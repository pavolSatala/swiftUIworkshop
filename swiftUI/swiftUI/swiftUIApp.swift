//
//  swiftUIApp.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import SwiftUI

@main
struct swiftUIApp: App {
    let episodeRepo    = DummyEpisodesRepo()
    let userRepo       = DummyUserRepo()
    
    var body: some Scene {
        WindowGroup {
            DashboardView(viewModel: DashboardViewModel(useCases: DashboardControllerUseCases(episodesRepo: self.episodeRepo, userRepo: self.userRepo)))
        }
    }
}
