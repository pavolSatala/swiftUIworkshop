//
//  View+Dimensions.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import SwiftUI

extension View
{
    var DEFAULT_PADDING : CGFloat {
        get {
            return 8
        }
    }
}
