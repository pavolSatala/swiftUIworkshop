//
//  DashboardCell.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import SwiftUI

struct DashboardCell: View {
    
    var data : DashboardCellData
    
    var body: some View {
        HStack(alignment: .center, spacing: DEFAULT_PADDING) {
            VStack(alignment: .leading, spacing: DEFAULT_PADDING) {
                Text(data.date)
                    .font(.headline)
                Text(data.user)
                    .foregroundColor(Color.red)
            }.padding(.leading, DEFAULT_PADDING)
            Spacer()
            VStack(alignment: .trailing, spacing: DEFAULT_PADDING) {
                Text(data.pulse)
                    .fontWeight(.heavy)
                Text(data.type)
                    .foregroundColor(Color.blue)
            }.padding(.trailing, DEFAULT_PADDING)
        }
    }
}

/*
struct DashboardCell_Previews: PreviewProvider {
    static var previews: some View {
        DashboardCell(data: DashboardCellData(date: "1.1.20022", user: "1", pulse: "12.23", type: "failure"))
    }
}
*/
