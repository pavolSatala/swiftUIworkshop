//
//  ContentView.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import SwiftUI

struct DashboardView<VM: DashboardViewModelProtocol>: View {
    
    @ObservedObject var viewModel : VM
    
    var body: some View {
        VStack(alignment: .center, spacing: DEFAULT_PADDING) {
            titleSelection
            ZStack {
                listView
                if (self.viewModel.loading) {
                    loading
                }
                
            }
            
        }.onAppear(perform: self.viewModel.onControllerAppeared)
    }
    
    var titleSelection : some View {
        HStack(alignment: .center, spacing: DEFAULT_PADDING) {
            Button("all") {
                self.viewModel.onTypeSelected(type: nil)
            }
            Spacer()
            Button("normal") {
                self.viewModel.onTypeSelected(type: .normal)
            }
            Spacer()
            Button("brad") {
                self.viewModel.onTypeSelected(type: .bradycardia)
            }
            Spacer()
            Button("tachy") {
                self.viewModel.onTypeSelected(type: .tachycardia)
            }
            Spacer()
            Button("fail") {
                self.viewModel.onTypeSelected(type: .failure)
            }
        }.padding(DEFAULT_PADDING)
    }
    
    var listView : some View {
        ScrollView {
            LazyVStack(alignment: .center, spacing: DEFAULT_PADDING) {
                ForEach( 0 ..< self.viewModel.numberOfRows, id: \.self) {
                    row in
                    DashboardCell(data: self.viewModel.getData(for: row))
                }
            }
        }
    }
    
    var loading : some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                ProgressView()
                    .progressViewStyle(.circular)
                Spacer()
            }
            Spacer()
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView(viewModel: DashboardViewModel(useCases: DashboardControllerUseCases(episodesRepo: DummyEpisodesRepo(), userRepo: DummyUserRepo())))
    }
}

