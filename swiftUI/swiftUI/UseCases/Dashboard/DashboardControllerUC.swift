//
//  DashboardControllerUC.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

protocol DashboardControllerUC
{
    func onControllerAppeared()
    func onTypeSelected(type: EpisodeType?)
    
    var episodesPublisher : AnyPublisher<[Episode]?, Never> { get }
    var userPublisher: AnyPublisher<Int?, Never>  { get }
}
