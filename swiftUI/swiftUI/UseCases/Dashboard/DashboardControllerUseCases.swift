//
//  DashboardControllerUseCases.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

class DashboardControllerUseCases : DashboardControllerUC
{
    internal var episodesRepo : EpisodesRepoProtocol!
    internal var userRepo : UserRepoProtocol!
    private var typeSubject : CurrentValueSubject<EpisodeType?, Never> = CurrentValueSubject<EpisodeType?, Never>(nil)
    
    var episodesPublisher: AnyPublisher<[Episode]?, Never> {
        get {
            episodesRepo.episodesSubject.combineLatest(userRepo.userSubject, typeSubject).map { (items, user, type) -> [Episode]? in
                
                let episodes = items?.filter({ episode in
                    return episode.userId == user && (type == nil || episode.type == type)
                })
                return episodes
            }.eraseToAnyPublisher()
                
        }
    }
    
    var userPublisher: AnyPublisher<Int?, Never> {
        get {
            userRepo.userSubject.eraseToAnyPublisher()
        }
    }
    
    init(episodesRepo : EpisodesRepoProtocol, userRepo : UserRepoProtocol ) {
        self.episodesRepo = episodesRepo
        self.userRepo = userRepo
    }
    
    func onControllerAppeared() {
        self.episodesRepo.startEpisodesListening()
        self.userRepo.startUserListening()
    }
    
    func onTypeSelected(type: EpisodeType?) {
        self.typeSubject.send(type)
    }
    
}
