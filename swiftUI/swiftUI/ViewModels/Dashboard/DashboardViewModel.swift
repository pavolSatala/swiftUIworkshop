//
//  DashboardViewModel.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import Combine

class DashboardViewModel : DashboardViewModelProtocol
{
    var useCases : DashboardControllerUC
    
    @Published var numberOfRows: Int = 0
    @Published var loading: Bool = true
    
    var cellItems : [DashboardCellData]?
    {
        didSet {
            self.numberOfRows = cellItems?.count ?? 0
        }
    }
    
    private var episodesSubscriber : AnyCancellable?
    
    init(useCases: DashboardControllerUC)
    {
        self.useCases = useCases
        
        self.episodesSubscriber = useCases.episodesPublisher.combineLatest(self.useCases.userPublisher).map({ (list, user) -> [DashboardCellData]? in
            return list?.map({ episode in
                var data = DashboardCellData()
                data.pulse = String(format: "%.2f", episode.avgPulse ?? 0.0)
                data.type = episode.type?.rawValue ?? ""
                data.user = "\(user ?? 0)"
                data.date = episode.date?.formatted() ?? ""
                return data
            })
        }).sink(receiveValue: { dataList in
            self.cellItems = dataList
            self.loading = (dataList?.count ?? 0) == 0
        })
    }
    
    func getData(for row: Int) -> DashboardCellData {
        return self.cellItems?[row] ?? DashboardCellData()
    }
    
    func onControllerAppeared() {
        self.useCases.onControllerAppeared()
    }
    
    func onTypeSelected(type: EpisodeType?) {
        self.useCases.onTypeSelected(type: type)
    }
    
    
}
