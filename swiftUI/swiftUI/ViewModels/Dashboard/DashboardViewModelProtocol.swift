//
//  DashboardViewModelProtocol.swift
//  swiftUI
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

struct DashboardCellData
{
    var date : String = ""
    var user : String = ""
    var pulse : String = ""
    var type : String = ""
}

protocol DashboardViewModelProtocol : ObservableObject {
    var numberOfRows : Int { get }
    var loading: Bool { get }
    func getData(for: Int) -> DashboardCellData
    
    func onControllerAppeared()
    func onTypeSelected(type: EpisodeType?)
}

